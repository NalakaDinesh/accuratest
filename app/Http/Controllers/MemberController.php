<?php

namespace App\Http\Controllers;

use App\Member;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    //
    public function store(Request $request){
        // dd($request->all());
        $this->validate($request,[
            'fName'=>'required',
            'lName'=>'required'
        ]);
        $member = new Member;
        $member->fName = $request->fName;
        $member->lName = $request->lName;
        $member->dob = $request->dob;
        $member->summery = $request->summery;
        $member->division = $request->division;
        $member->save();
        $members = Member::all();
        return redirect()->to('/home')->with('members',$members);
    } 

    public function deleteMember($id){

        $member = Member::find($id);
        $member->delete();
        return redirect()->back();
    }

    public function editMember($id){

        $member = Member::find($id);
        // dd($member);
        return view('editMember')->with('member',$member);
    }

    public function updateMember(Request $request,$id){
        dd($request);
        $member = Member::find($id);
        $member->fName = $request->fName;
        $member->lName = $request->lName;
        $member->dob = $request->dob;
        $member->summery = $request->summery;
        $member->save();
        // $member = Member::find($id);
        // $members = Member::all();
        // return redirect()->to('/home')->with('members',$members);
    }
}
