<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <br><br><br>
  @foreach ($errors->all() as $error)
      <div class="alert alert-danger" role="alert">
        {{$error}}
      </div>
  @endforeach
  <br><br>
  <form action="/updateMember" method="post" id="memberForm">
    {{@csrf_field()}}
    <div class="row">

        <div class="col-md-6">
            <div class="form-group">
                <label for="fName">First Name:</label>
                <input type="text" class="form-control" id="fName"  name="fName" value="{{$member->fName}}">
              </div>
              <div class="form-group">
                <label for="lName">Last Name:</label>
                <input type="text" class="form-control" id="lName" name="lName" value="{{$member->lName}}">
              </div>
              <div class="form-group">
                <label for="summery">Summery:</label>    
              </div>
              <textarea rows="4" cols="50" name="summery" form="memberForm"> {{$member->summery}}</textarea>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="division">Division:</label>
                  <select name="division" id="division" class="form-control">
                    <option value="{{$member->division}}" class="form-control">{{$member->division}}</option>
                    <option value="Colombo1" class="form-control">Colombo1</option>
                    <option value="Colombo2" class="form-control">Colombo2</option>
                    <option value="Colombo3" class="form-control">Colombo3</option>
                  </select>
              </div>
              <div class="form-group">
                <label for="dob">Date of Birth:</label>
                <input type="date" class="form-control" id="dob"  name="dob" value="{{$member->dob}}">
              </div>
             
        </div>
    </div>
    <br>
    
    <button type="button" class="btn btn-warning" style="display: inline-block;" >Reset</button>
    <button type="submit" class="btn btn-success" style="display: inline-block;" >Update</button>
    {{-- <button type="button" class="btn btn-default" style="display: inline-block;">Save</button> --}}
  </form>
</div>

</body>
</html>
