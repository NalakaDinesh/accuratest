<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <br><br><br>
    <h2 class="text-center">Member List</h2>
    <br>
    <input type="text" class="btn btn-default"  placeholder="Search">
    <a href="/member" class="btn btn-success" style="float: right">Add New Member</a>
  
  <br><br>
  <form action="/action_page.php" id="memberForm">
    <div class="row">
        <table class="table table-dark">
          <thead>
            <th>First</th>
            <th>Last</th>
            <th>Division</th>
            <th>Date of Birth</th>
            <th>Actions</th>
            </thead>
            <tbody>
              @foreach ($members as $member)                
              <tr>
                <td>{{$member->fName}}</td>
                <td>{{$member->lName}}</td>
                <td>{{$member->dob}}</td>
                <td>{{$member->division}}</td>
                <td>
                  <a href="/editMember/{{$member->id}}" class="btn btn-info">Edit</a>
                  <a href="/deleteMember/{{$member->id}}" class="btn btn-danger">Delete</a>
                </td>
              </tr>
              @endforeach
            </tbody>
        <table>
    </div>
    <br>
  
  </form>
</div>

</body>
</html>
