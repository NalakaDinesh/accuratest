<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data= App\Member::all();
    // dd($data);
    return view('home')->with('members',$data);
});


Route::get('/member', function () {
    return view('member');
});

Route::get('/home', function () {
    $data= App\Member::all();
    // dd($data);
    return view('home')->with('members',$data);
});

Route::post('/saveMember', 'MemberController@store');
Route::get('/deleteMember/{id}', 'MemberController@deleteMember');
Route::get('/editMember/{id}', 'MemberController@editMember');
Route::put('/updateMember/{id}', 'MemberController@updateMember');